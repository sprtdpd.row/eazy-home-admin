/*
* Arduino code
* This is new Arduino code under testing.
* There is no need to configure your arduino here.
* Only Passive device is now Supported. Soon active device support will be made into the server.
* This code is also Licensed under GPL license. You are free to use the code as you wish.
*/

// These function will initialize the pin and control it inside the loop function
void digital_write(int pin, int state){   
  pinMode(pin,OUTPUT);
  digitalWrite(pin,state);
 
  
}

int digital_read(int pin){   
  pinMode(pin,INPUT);
  int state = digitalRead(pin);
  return state; 

}

void analog_write(int pin, int state){   
  pinMode(pin,OUTPUT);
  analogWrite(pin,state);
  
}

int analog_read(int pin){   
  pinMode(pin,INPUT);
  int state = analogRead(pin);
  return state; 

}

void setup() {
 
  Serial.begin(9600);
}

void loop() {
  if (Serial.available() > 0){
    String x = Serial.readString(); // read string from the serial port
    x.trim(); // trim spaces
    int loc =  x.indexOf(':'); // finding the index of ':'
    if (loc <=  0){
          Serial.println("Invalid Arugment supplied.");
    }
        else {

      String split[2]; // a variable to contain split values
      split[0] = x.substring(0, loc); // the part before :
      split[1] = x.substring(loc + 1); // the part after :
      if (split[0].equals("read")) { // System call to read status of device.
        int dev = split[1].toInt();
        String data = String(digital_read(dev));
        Serial.println(data);
  

      }
      //here we turn on or off the device connected to device.
      else{
        int pin = split[0].toInt();
   
        if(split[1].equals("on")){ 
           digital_write(pin, HIGH);
           Serial.println("The Device has been turned on ");
        }
        else if (split[1].equals("off")){
           digital_write(pin, LOW) ;
           Serial.println("The Device has been turned off"); 
        }

          
        }
      }
      
    }


}
