"""
* This will be future of arduino controlling. There will be a universal code to control any sort of device soon.
* This works with legacy and new arduino code.
* This is noting but blind copy of arduino.py with new protocols.
* This can be used when you want a custom arduino code. (Recommended For arduino Developers).
* This will be user friendly mode. You must define pin number for each devices when configuring names.
* The arduino code can be tailored as you wish depending on amount of devices that you are going to control using it.
* For configuration, Fill in the fields given on arduino sketch and assign those devices on "devices.yaml" file using web interface.
* You can also edit the devices.yaml file but be sure to follow the format.
* Other modes can get removed after defined testing of this.
"""

import time
import traceback
import os
from store import list_devices_name, get_pin_number, get_device_status, change_device_status
import serial
import yaml
import sys
from store import getConfig, create_config_file

data = None
if os.path.exists('./config.yaml'):
    with open('./config.yaml') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
else:
    create_config_file()


def list_serial_ports():
    import serial.tools.list_ports
    """ Lists serial port names
        :returns:
            A list of the serial ports available on the system
    """
    available_ports = list(serial.tools.list_ports.comports())
    return available_ports


def arduino_write(serial_port, data_string):
    """
    A function to send a string to Arduino.
    :rtype: String
    :param: data String
    :param: s pySerialObject
    :returns: String, Boolean

    """
    try:
        serial_port.write(data_string.encode())

        x = serial_port.readline().decode('utf-8')
        print(x)
        return x, True
    except Exception as e:
        print(e)
        return e, False


def toggle_device(board, device_name, device_status):
    """
    A function to turn on/off the pins on Arduino.
    :param board:  pySerial Object
    :param device_name: String
    :param device_status: String
    :return:
    """
    l_devices = list_devices_name()
    if device_name in l_devices:
        pin = get_pin_number(device_name)

        try:
            if (device_status == 'on' and get_device_status(device_name)) == 'off' or (
                    device_status == 'off' and get_device_status(device_name) == 'on'):
                data = f'{pin}:{device_status}'
                arduino_write(board, data)
                print(f"{pin} turned {device_status}")
                change_device_status(device_name, device_status)
                return f'Info: Device turned {device_status} successfully', True
            else:
                print(f"Error: The device is already turned {device_status}")
                return f"Error: The device is already turned {device_status}"

        except:
            traceback.print_exc()
    else:
        return 'Error: Device not found', False


def toggle_door(board, cmd):
    """
    A function to lock or unlock the door.
    :param board:
    :param cmd:
    :return:
    """
    conf = getConfig()
    door_pin = int(conf[7])
    try:
        if cmd == 'ul':
            arduino_write(board, f"{door_pin}:on")
        elif cmd == 'l':
            arduino_write(board, f"{door_pin}:off")
        return True
    except:
        return False


def connect(port):
    s = serial.Serial(port, baudrate=9600, timeout=2)
    time.sleep(3)
    return s


if __name__ == '__main__':
    arduino = connect(data["arduinoport"])
    if len(sys.argv) >= 2:
        if sys.argv[1] == "test":
            arduino_write(arduino, "test")
        if sys.argv[1] == "toggle":
            try:
                pin = sys.argv[2]
                status = sys.argv[3]
                stuff = f'{pin}:{status}'
                arduino_write(arduino, stuff)
            except:
                print("Error. Check your argument.")
    else:
        ports = list_serial_ports()
        for p in ports:
            print(p)
